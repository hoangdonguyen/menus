<?php

/**
 *
 * Copyright (c) 2016 by Turner Broadcasting System, Inc.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Turner Broadcasting System,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Turner Broadcasting System.
 *
 * @Project: Marty
 * @Author:  hnguyen
 * @Version: 1.0
 * @Description:  php script opens the NBA Drupal admin page and clicks
 * 				  the log in link
 *
 * @Date created: Mar 21, 2016
 *
 */

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;

use Behat\Mink\Driver\Selenium2Driver;
use Selenium\Client as SeleniumClient;

use Behat\Mink\Session;
use Behat\Mink\Selector\Xpath\Escaper;

require __DIR__ . '\..\..\vendor\autoload.php';
require __DIR__ . '\..\Helpers\SearchHelper.php';
require __DIR__ . '\..\Helpers\ClickHelper.php';
require __DIR__ . '\..\Helpers\DataEntryHelper.php';
require __DIR__ . '\..\Helpers\UploadHelper.php';

//require_once('SearchHelper.php');

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends PHPUnit_Framework_TestCase implements Context, SnippetAcceptingContext
{
    private $driver;
    private $session;
    private $xpathEscaper;
    private $file;
    static $logFile;
    private $handle;
    private $currentDate;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        global $logFile;

        parent::__construct();
        $this->driver = new Selenium2Driver('chrome');

        // by default,if there is no parameter passing in Selenium2Driver(),
        // firefox browser is used
        //
        //$this->driver = new Selenium2Driver();

        $this->session = new Session($this->driver);
        $this->xpathEscaper = new Escaper();

        $this->file = $logFile;
    }

    /**
     * @BeforeSuite
     *
     * This function sets up the environment for the test:
     *      - gets the log file name, the selenium standalone server and the browser from the file config.ini
     *      - opens the log file to write to it
     *      - starts the selenium standalone server
     *
     */
    public static function startSeleniumServer()
    {
        global $logFile;

        // martyLog.txt is a log file for troubleshooting the script
        // if there is something wrong.  The file path myLogFile is
        // defined in config.ini file.
        $ini_array = parse_ini_file('/../config.ini');

        $logFile        = $ini_array['myLogFile'];
        $SeleniumServer = $ini_array['mySeleniumServer'];
        $ChromeDriver   = $ini_array['myChromeDriver'];

        file_put_contents($logFile, LOCK_EX);

        $handle = fopen($logFile,'a') or die('Cannot open file:  '.$logFile);

        date_default_timezone_set('America/New_York');

        // starts the selenium server in Windows.  If this test suite is run
        // in other OS, COM cannot be used.
        $WshShell = new COM("WScript.Shell");

        //$oExec = $WshShell->Run('java -jar "C:\Users\hnguyen\Downloads\selenium-server-standalone-2.53.0.jar" -Dwebdriver.firefox.bin="C:\Program Files\Mozilla Firefox\firefox.exe"', 10, false);
        //$oExec = $WshShell->Run("java -jar C:\Users\hnguyen\Downloads\selenium-server-standalone-2.53.0.jar -Dwebdriver.chrome.driver=C:\Users\hnguyen\Downloads\chromedriver_win32\chromedriver.exe", 10, false);
        $oExec = $WshShell->Run("java -jar $SeleniumServer -Dwebdriver.chrome.driver=$ChromeDriver");

        $currentDate = date('Y-m-d H:i:s');
        sleep(3);       // slow down so that the selenium can start completely

        if ($oExec == 0)
            $current = "\r\n" .$currentDate. " startSeleniumServer():: " . $oExec . "\r\n";
        else
            $current = "\r\n" .$currentDate. " startSeleniumServer():: Cannot start the selenium server.\r\n";

        // Write to the log file
        file_put_contents($logFile, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Given I go to :aHomePage
     *
     * This function opens a web site.  In Mink, the entry point
     * to the browser is called the session
     *
     * @param $aHomePage
     *
     */
    public function openWebSite($aHomePage)
    {

        // start the session and open the web site
        $this->session->start();
        $this->session->visit($aHomePage);
        $this->session->resizeWindow(1440, 900, 'current');

        // open the log file in append mode
        $this->handle = fopen($this->file,'a') or die('Cannot open file:  '.$this->file);
        //error_log($this->handle, 3, 'C:\errorLog.txt');

        $this->currentDate = date('Y-m-d H:i:s');

        // note that selenium2 does not provide the http status code.
        // The only way to verify that a page can be opened is to check
        // if something is found on that page, eg. Log In link
        $current = "\r\n" .$this->currentDate. " openWebSite()::URL: " . $this->session->getCurrentUrl() . "\r\n";
        $current .= "\r\n" .$this->currentDate. " openWebSite()::Window Name: " . $this->session->getWindowName() . "\r\n";

        // Write to the log file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @When I see the :aLink link
     *
     * This function looks up a link
     *
     * @param $aLink
     *
     */
    public function locateTheLink($aLink)
    {
        
        // get the page document
        $page = $this->session->getPage();
        $current = null;

        // search the page document for a  specific link
        sleep(2);
        $linkObj = searchLink($page, $aLink);

        $this->currentDate = date('Y-m-d H:i:s');

        if (is_null($linkObj))
            $current = "\r\n" .$this->currentDate. " locateTheLink()::The link '" .$aLink. "' cannot be found.";
        else {
            $current = "\r\n" . $this->currentDate . " locateTheLink()::The link '" . $linkObj->getText() . "' is found.";

            $linkHref = $linkObj->getAttribute('href');
            $current .= "\r\n" . $this->currentDate . " locateTheLink()::The link href '" . $linkHref . "' is found.\r\n";
        }

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Given I search for the :aLink link by clicking the :aToolBarIcon toolbar icon
     *
     * This function looks up a link which is in the toolbar
     *
     * @param $aLink
     * @param $aToolBarIcon
     *
     */
    public function searchTheLink($aLink, $aToolBarIcon)
    {
        // get the page document
        $page = $this->session->getPage();

        // search the page document for a  specific link
        $linkObj = searchLink($page, $aLink);

        $this->currentDate = date('Y-m-d H:i:s');

        if (is_null($linkObj)) {
            $current = "\r\n" . $this->currentDate . " searchTheLink()::The link '" . $aLink . "' cannot be found.  Need to click the toolbar icon";
            $toolBarIconName = searchUsingXpath($page, $aToolBarIcon);

            if ($toolBarIconName) {
                $current .= "\r\n" .$this->currentDate. " searchTheLink()::The toolbar icon " .$toolBarIconName->getText(). " is found.";
                $toolBarIconName->click();
            } else
                $current .= "\r\n" .$this->currentDate. " searchTheLink()::The toolbar icon " .$toolBarIconName->getText(). " cannot be found.";

        } else {
            $current = "\r\n" . $this->currentDate . " searchTheLink()::The link '" . $aLink . "' is found.  There is no need to click " .$aToolBarIcon;
        }
        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @When I see the subnav :aLink link
     *
     * This function looks up the subnav link in the header
     *
     * @param $aLink
     *
     */
    public function locateSubNavLink($aLink)
    {
        $isElementFound = false;

        // get the page document
        $page = $this->session->getPage();

        $this->currentDate = date('Y-m-d H:i:s');

        // because there are a few links under the class admin-list, findAll() is
        // used.  It returns an array of links.  findAll('css', '.nba-secondary-nav__list--separator') searches
        // for the class of nba-secondary-nav__list--separator because this is clickable
        //
        // <ul class="nba-nav__container--center-menu show-for-medium" data-dropdown-menu="k3m6qk-dropdown-menu" role ="menubar">
        //   <li class="nba-secondary-nav__list--separator nba-nav__container--center-menu-item" role="menuitem">
        //      <a href="http://www.nba.com/gameline/20160505/"
        //		 title="scores schedules" tabindex="0">Scores & Schedules
        //      </a>
        //   </li>
        //   <li> .... </li>
        //   <li> .... </li>
        $hrefOfElement = $page->findAll('css', '.nba-secondary-nav__list--separator');

        foreach ($hrefOfElement as $hrefOfElement) {
            if ($hrefOfElement->getText() === $aLink) {
                $current = "\r\n" . $this->currentDate . " locateSubNavLink()::The subnav '" . $hrefOfElement->getText() . "' link is found on the current page.";
                $isElementFound = true;
                break;
            }
        }
        if (! $isElementFound)
            $current = "\r\n" .$this->currentDate. " locateSubNavLink():: Element '" .$aLink. "' cannot be found on the current page.";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
        sleep(1);
    }

    /**
     * @When I click the :aLink link
     *
     * This function clicks a link
     *
     * @param $aLink
     *
     */
    public function clickTheLink($aLink)
    {
        sleep(1);
        // get the page document
        $page = $this->session->getPage();
        $this->currentDate = date('Y-m-d H:i:s');

        $linkObj = searchLink($page, $aLink);
        
        if (is_null($linkObj)) {
            $current = "\r\n" .$this->currentDate. " clickTheLink()::The link '" .$aLink. "' cannot be clicked because it is not found on the page.\r\n";
        } else {
            //clickALink($page, $aLink);
            $linkObj->click();
            $current = "\r\n" .$this->currentDate. " clickTheLink()::The link '" .$aLink. "' is clicked\r\n";
        }

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Then I click the back arrow of browser
     *
     * This function clicks the back arrow to go back to the previous page
     *
     */
    public function clickTheBackLink()
    {
        $this->driver->back();
    }

    /**
     * @When I click the :aLink link whose the class is :aLinkClass
     *
     * This function clicks a link
     *
     * @param $aLink
     * @param $aLinkClass
     *
     */
    public function clickTheClassLink($aLink, $aLinkClass)
    {
        // get the page document
        $page = $this->session->getPage();

        $this->currentDate = date('Y-m-d H:i:s');

        if (is_null($page->find('css',".$aLinkClass"))) {
            $current = "\r\n" .$this->currentDate. " clickTheLink()::The link '" .$aLink. "' cannot be clicked because it is not found on the page.\r\n";
        } else {
            clickAClassLink($page, $aLinkClass);
            $current = "\r\n" .$this->currentDate. " clickTheLink()::The link '" .$aLink. "' is clicked\r\n";
        }

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @When I click the :aLink link from a list of link
     *
     * This function clicks a link from a
     *
     * @param $aLink
     *
     */
    public function clickItemFromList($aLink)
    {
        $isElementFound = false;

        // get the page document
        $page = $this->session->getPage();

        $this->currentDate = date('Y-m-d H:i:s');

        // because there are a few links under the class admin-list, findAll() is
        // used.  It returns an array of links.  findAll('css', '.label') searches
        // for the class of label because this is clickable
        //
        // <ul class="admin-list">
        //   <li>
        //      <a href="/admin/structure/block">
        //        <span class="label">Block layout</span>
        //        <div class="description">.....</div>
        //      </a>
        //   </li>
        //   <li> .... </li>
        //   <li> .... </li>
        $hrefOfElement = $page->findAll('css', '.label');

        foreach ($hrefOfElement as $hrefOfElement) {
            if ($hrefOfElement->getText() === $aLink) {
                $current = "\r\n" . $this->currentDate . " clickItemFromList()::'" . $hrefOfElement->getText() . "' is found on the current page.";
                $hrefOfElement->click();
                $current .= "\r\n" . $this->currentDate . " clickItemFromList():: just clicked the link.";
                $isElementFound = true;
                break;
            }
        }
        if (! $isElementFound)
            $current = "\r\n" .$this->currentDate. " clickItemFromList():: Element '" .$aLink. "' cannot be found on the current page.";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
        sleep(1);
    }
    
    /**
     *
     * @Then I see :aString of element :htmlElement in the toolbar region
     *
     * @param $aString
     * @param $htmlElement
     *
     */
    public function lookUpTextInToolbar($aString, $htmlElement)
    {
        sleep(2);           // sometimes the response is a little bit slow

        $page = $this->session->getPage();
        $opStatus = searchToolbarRegion($page, $htmlElement);

        $this->currentDate = date('Y-m-d H:i:s');

        if ($opStatus) {
            $current = "\r\n" .$this->currentDate. " lookUpTextInToolbar():: A text of '" .$opStatus->getText(). "' is found on the toolbar region.";
        }
        else
            $current = "\r\n" .$this->currentDate. " lookUpTextInToolbar():: A text of '" .$aString. "' cannot be found on the toolbar region.";

        $this->assertContains($aString, $opStatus->getText());
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);

    }

    /**
     *
     * @Then I click the dropdown button of element :htmlElement
     *
     * @param $htmlElement
     *
     */
    public function searchDropDown($htmlElement)
    {
        $page = $this->session->getPage();

        //$hrefOfDropdown = $page->find('css', $htmlElement);
        $hrefOfDropdown = searchToolbarRegion($page, $htmlElement);
        if ($hrefOfDropdown->isVisible())
            $current = "\r\n" .$this->currentDate. " searchDropDown():: An href of '" .$hrefOfDropdown->getAttribute('href'). "' is found on the current page.";
        else
            $current = "\r\n" .$this->currentDate. " searchDropDown():: A text of '" .$htmlElement. "' cannot be found on the current page.";

        $this->clickTheDropDownMenu($hrefOfDropdown->getAttribute('href'));
        
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     *
     * @Given I click the dropdown arrow whose the xpath is :dropDownArrowXpath to look for :anItem to click
     *
     * @param $dropDownArrowXpath
     * @param $anItem
     *
     * This function clicks on the arrow of a dropdown menu to
     * get an item and then click on that item
     * 
     */
    public function dropDownArrow($dropDownArrowXpath, $anItem)
    {
        $page = $this->session->getPage();

        // look for the dropdown arrow.  Because there might be
        // other dropdown arrows in the page, xpath helps to select
        // the right one
        $myLinkXpath = $page->find('xpath', $dropDownArrowXpath);
        
        if ($myLinkXpath) {
            $current = "\r\n" .$this->currentDate. " dropDownArrow():: " .$myLinkXpath->getAttribute('class'). " arrow is found";

            // click the dropdown arrow so that $anItem
            // can be visible
            $myLinkXpath->press();

            // look for the $anItem in the dropdown menu
            $xpathEscaper = new Escaper();
            $anXpath = $xpathEscaper->escapeLiteral($anItem);
            $aText = $page->find('named', array('button', $anXpath));

            if ($aText->isVisible()) {
                $current .= "\r\n" . $this->currentDate . " dropDownArrow():: '" . $aText->getAttribute('value') . "' is found";
                $aText->click();
            } else
                $current = "\r\n" .$this->currentDate. " dropDownArrow():: $anItem is not found in the dropdown menu";

        } else
            $current = "\r\n" .$this->currentDate. " dropDownArrow():: dropdown arrow is not found ";

        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Then I see the :aText at the xpath of :anXpath
     *
     * @param $aText
     * @param $anXpath
     *
     */
    public function itemCreated($aText, $anXpath) {
        sleep(2);       // slow down a bit to wait for the page

        $page = $this->session->getPage();

        $myLinkXpath = searchXpath($page, $anXpath);

        if ($myLinkXpath) {
            $current = "\r\n" . $this->currentDate . " itemCreated():: Parent=" . $myLinkXpath->getParent()->getText();
            
            //if ($this->assertContains($aText ,$myLinkXpath->getParent()->getText()))
            //    $current .= "\r\n" . $this->currentDate . " itemCreated()::'" .$aText. "' is found.";
        } else
            $current = "\r\n" . $this->currentDate . " itemCreated():: xpath is not found";

        // verify that the input string matches the display string
        $this->assertContains($aText ,$myLinkXpath->getParent()->getText());
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * 
     * @Then I see :aString whose id is :anId on the current page
     *
     * @param $aString
     * @param $anId
     *
     */
    public function lookUpText($aString, $anId)
    {
        $page = $this->session->getPage();
        $aText = searchUsingId($page, $anId);

        $this->currentDate = date('Y-m-d H:i:s');

        if ($aText) {
            $current = "\r\n" .$this->currentDate. " lookUpText():: A text of '" .$aText->getText(). "' is found on the current page.";
        }
        else
            $current = "\r\n" .$this->currentDate. " lookUpText():: A text of '" .$aString. "' cannot be found on the current page.";

        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);

        $this->assertSame($aString, $aText->getText());

    }

    /**
     * @When I see :aString in the table on the current page
     * 
     * @param $aString
     * 
     */
    public function lookUpStringInTable($aString)
    {
        $page = $this->session->getPage();

        $this->currentDate = date('Y-m-d H:i:s');

        $stringInTable = searchUsingXpath($page, $aString);
        
        if ($stringInTable)
            $current = "\r\n" .$this->currentDate. " lookUpStringInTable()::The '" .$stringInTable->getText(). "' is found.";
        else
            $current = "\r\n" . $this->currentDate . " lookUpStringInTable()::The '" .$aString. "' cannot be found.";
    
        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }
    
    /**
     * @When I enter :fieldData in the :fieldName input box
     *
     * @param $fieldData
     * @param $fieldName
     *
     */
    public function populateTheInputBox($fieldData, $fieldName)
    {
        $page = $this->session->getPage();
        dataEntry($page, $fieldName, $fieldData);

        $this->currentDate = date('Y-m-d H:i:s');

        // Append to the log file
        $current = "\r\n" .$this->currentDate. " populateTheInputBox()::".$fieldName. ": " .$fieldData;
        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);

        sleep(2);
    }

    /**
     * @When I see the :aButton button
     * 
     * @param  $aButton
     * 
     */
    public function locateTheButton($aButton)
    {
        // get the page document
        $page = $this->session->getPage();

        $foundButton = searchButton($page, $aButton);

        $this->currentDate = date('Y-m-d H:i:s');
        if (is_null($foundButton))
            $current = "\r\n" .$this->currentDate. " locateTheButton()::The button '" .$foundButton->getValue(). "' cannot be found.";
        else {
            $current = "\r\n" . $this->currentDate . " locateTheButton()::The button '" . $foundButton->getValue(). "' is found.";
        }
        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Given I see the cell name :aCell on the current page
     *
     * @param $aCell
     *
     */
    public function lookUpCell($aCell)
    {
        $page = $this->session->getPage();
        $cellName = searchCell($page, $aCell);

        $this->currentDate = date('Y-m-d H:i:s');
        if (!$cellName)
            $current = "\r\n" . $this->currentDate . " lookUpCell()::The menu name " . $cellName->getText() . " cannot be found.\r\n";
        else {
            $current = "\r\n" . $this->currentDate . " lookUpCell()::The menu name " . $cellName->getText() . " is found.\r\n";
        }
        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @When I press the :aButton button
     *
     * @param $aButton
     *
     */
    public function pressTheButton($aButton)
    {
        $this->currentDate = date('Y-m-d H:i:s');

        $page = $this->session->getPage();
        pressAButton($page, $aButton);

        $current = "\r\n" . $this->currentDate . " pressTheButton()::The button '" .$aButton. "' is pressed.\r\n";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }
    
    /**
     * @When I click the :aToolBarIcon toolbar icon
     *
     * @param $aToolBarIcon
     *
     */
    public function clickTheToolbarIcon($aToolBarIcon)
    {
        // Open the log file to get existing content
        $this->currentDate = date('Y-m-d H:i:s');

        $page = $this->session->getPage();

        $toolBarIconName = searchUsingXpath($page, $aToolBarIcon);

        if ($toolBarIconName) {
            $current = "\r\n" .$this->currentDate. " clickTheToolbarIcon()::The toolbar icon " .$toolBarIconName->getText(). " is found.";
            $toolBarIconName->click();
        } else
            $current = "\r\n" .$this->currentDate. " clickTheToolbarIcon()::The toolbar icon " .$toolBarIconName->getText(). " cannot be found.";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
        sleep(1);
    }

    /**
     * @Then I click the dropdown button of the href :aHref
     * 
     * @param $aHref
     * 
     */
    public function clickTheDropDownMenu($aHref)
    {

        $page = $this->session->getPage();

        clickADropDownMenu($page, $aHref);

        $this->currentDate = date('Y-m-d H:i:s');

        $current = "\r\n" .$this->currentDate. " clickTheDropDownMenu()::The dropdown menu is found and clicked.\r\n";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Then I click the dropdown button of the xpath :linkXpath
     *
     * @param $linkXpath
     *
     */
    public function deleteLink($linkXpath) {
        $page = $this->session->getPage();
        $myLinkXpath = $page->find('xpath', $linkXpath);
        $aHref = $myLinkXpath->getAttribute('href');

        $this->currentDate = date('Y-m-d H:i:s');

        if ($myLinkXpath) {
            $current = "\r\n" .$this->currentDate. " deleteLink()::The href of the link is " .$aHref. "\r\n";
            clickADropDownMenu($page, $aHref);
        } else
            $current = "\r\n" .$this->currentDate. " deleteLink()::The href of the link cannot be found.\r\n";

        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     *
     * @Then I upload the :anImg image :imgLoc
     * 
     * @param $anImg
     * @param $imgLoc
     * 
     */
    public function uploadImg($anImg, $imgLoc) {
        $page = $this->session->getPage();
        uploadThing($page, $imgLoc);

        // check if img is uploaded successfully
        $isImgUploaded = $page->find('xpath',"//a[text()='$anImg']");
        $this->currentDate = date('Y-m-d H:i:s');

        if ($isImgUploaded)
            $current = "\r\n" .$this->currentDate. " uploadImg()::The img " .$isImgUploaded->getText(). " is uploaded.\r\n";
        else
            $current = "\r\n" .$this->currentDate. " uploadImg()::The img " .$isImgUploaded->getText(). " cannot be uploaded.\r\n";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Then I select the parent link of :aParentLink
     *
     * @param $aParentLink
     *
     */
    public function selectParentLink($aParentLink)
    {
        $page = $this->session->getPage();

        $selectField = searchOptionOfDropdownMenu($page, $aParentLink);
        $this->currentDate = date('Y-m-d H:i:s');
        
        if ($selectField) {
            $selectField->click();
            $current = "\r\n" .$this->currentDate. " selectParentLink()::The parent link of " .$selectField->getValue(). " is selected.\r\n";
        } else
            $current = "\r\n" .$this->currentDate. " selectParentLink()::The parent link of " .$selectField->getValue(). " cannot be selected.\r\n";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
    * @Then I click the dropbutton-wrapper
     *
     * Note: this method has not been fully implemented/tested yet.  A menu
     *       cannot be deleted by selecting the delete link from the dropdown
     *       menu in teh Menu page because the button does not have any id/value.
     *       It should be able to delete by specifying the parent classes but
     *       at the mean time, i don't know how to do that (04/19/2016)
    */
    public function dropDownWrapper()
    {
        $page = $this->session->getPage()->findField('Delete menu');

        if (empty($page))
            $current = "\r\nThe field does not have options selected.\r\n";
        else
            $current = "\r\nThe field has any options selected.\r\n";

        $page->findAll('xpath', '//option[@selected="Delete menu"]');
 
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);

    }
    
    /**
     * @When I close the web browser
     */
    public function closeTheWebBrowser()
    {
        file_put_contents($this->file, "\r\n--------------------------------------", FILE_APPEND | LOCK_EX);
        // close the log file
        fclose($this->handle);
        $this->session->stop();
    }

    /**
     * @AfterSuite
     */
    public static function stopSeleniumServer()
    {
        $WshShell = new COM("WScript.Shell");
        $WshShell->Run("Taskkill /F /IM java.exe", 10, false);
    }

}