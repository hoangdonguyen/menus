<?php
/**
 * Created by PhpStorm.
 * User: hnguyen
 * Date: 4/6/2016
 * Time: 11:56 AM
 */

use Behat\Mink\Selector\Xpath\Escaper;

/**
 *
 * This function looks up a link on a page
 *
 */
function searchLink($page, $aLink)
{
    // there are 2 ways to search for a link:
    //      - use find()
    //      - use findLink()
    //
    // to use find(), the xpath of a link must be determined as follows:
    //      $anXpath = $this->xpathEscaper->escapeLiteral($aLink);
    //      $this->linkEl = $page->find('named', array('link', $anXpath));

    // Append to the log file
    //$current .= "\r\n\r\nlocateTheLink()::The link href of " .$aLink. " is: ". $this->linkEl->getAttribute('href') . "\r\n";

    $linkObj = $page->findLink($aLink);
    //$linkObj = $page->find('css', sprintf('a:contains("%s")',$aLink));

    return $linkObj;
}

/*
 *
 * This function finds a specific string on a page
 *
 */
function searchUsingXpath($page, $aString)
{
    $xpathEscaper = new Escaper();
    
    // look up a text using xpath.  If $aString = "a string",
    // $anXpath will be a string (without quotes)
    $anXpath = $xpathEscaper->escapeLiteral($aString);
    $aText = $page->find('named', array('content', $anXpath));
    
    return $aText;
}

/*
*
* This function finds a specific string on a page
*
*/
function searchUsingId($page, $anId)
{
    $aText = $page->findById($anId);
    return $aText;
}

/*
 *
 * This function finds a specific button on a page
 *
 */
function searchButton($page, $aButton)
{
    $foundButton = $page->findButton($aButton);
    return $foundButton;
}

/*
 *
 * This function finds a specific cell on a page
 *
 */
function searchCell($page, $aCell)
{
    $cellName = $page->find('css', sprintf('table tbody tr td:contains("%s")', $aCell));
    
    return $cellName;
}

/*
 *
 * This function finds a specific button on a page
 *
 */
function searchOptionOfDropdownMenu($page, $anOption)
{
    // Note: the ',' after $anOption should not be removed !!
    //
    $selectField = $page->findField("edit-menu-parent")->find('named', array('option', $anOption,));
    
    return $selectField;
}

/*
 *
 * This function finds a field name on a page
 *
 */
function searchField($page, $formItem, $namedSelector)
{
    //$aFieldName = $page->findField($fieldName);
    $aFieldName = $page->find('css', ".$formItem $namedSelector");
    return $aFieldName;
}

/*
 *
 * This function 
 *
 */
function searchToolbarRegion($page, $htmlElement)
{
    $opStatus = $page->find('css', $htmlElement);
    return $opStatus;
}

/*
 *
 * This function finds an xpath in a page
 *
 */
function searchXpath($page, $anXpath)
{
    $anItem = $page->find('xpath', $anXpath);
    return $anItem;
}