Feature: NBA Drupal Admin Page
  In order to work on the NBA Admin page
  As a editor
  I need to be able to use NBA Admin page powered by Drupal 8

  Background:
    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
    #######
    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
    And I see the "Log in" link
    Then I click the "Log in" link

    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
    When I enter "marty" in the "Username" input box
    And I enter "NBAmarty-2016" in the "Password" input box
    Then I press the "Log in" button
    And I see the "My account" link


  Scenario Outline: verify that a few menus can be added

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    And I see the "Add menu" link
    When I click the "Add menu" link
    Then I see "Add menu" whose id is "block-seven-page-title" on the current page

    Given I enter "<Menu Name>" in the "Title" input box

    ## a default machine name can be editted by clicking the Edit link.  This is done
    ## in this scenario because this link is not clicked, when the script executes the
    ## pressing button, it actually click on the Edit link !!
    And I click the "Edit" link whose the class is "link"

    And I enter "<Menu Admin Summary>" in the "Administrative summary" input box
    When I press the "Save" button
    And I see "<Status Message>" of element "div.messages--status" in the toolbar region

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
      |Menu Name|Menu Admin Summary|Status Message             |
      |1-Menu   |Menu1 admin sum   |Menu 1-Menu has been added.|
      |2-Menu   |Menu2 admin sum   |Menu 2-Menu has been added.|

  Scenario Outline: verify that 7 links can be added to a menu

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "1-Menu" on the current page
    Then I click the dropdown button of the href "/admin/structure/menu/manage/1-menu"

    Given I click the "Add link" link
    When I see "Add menu link" whose id is "block-seven-page-title" on the current page
    Then I enter "<Link Title>" in the "Menu link title" input box
    And I enter "<Link Address>" in the "Link" input box
    And I enter "<Link Description>" in the "Description" input box

    ## icon with img feature has not been implemented yet
    ##And I upload the "<Image Name>" image "<Image Location>"

    And I select the parent link of "<Parent Link>"
    And I press the "Save" button

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
    |Link Title        |  Link Address                                |Link Description|Image Name     |Image Location                           |Parent Link|
    |Tickets           |http://nbatickets.nba.com/?ls=iref:nba:gnav   |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |Teams             |http://www.nba.com/teams/?ls=iref:nba:gnav    |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |Scores & Schedules|http://www.nba.com/gameline/20160413/         |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |News              |http://www.nba.com/news/?ls=iref:nba:gnav     |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |Playoffs          |http://www.nba.com/playoffs/?ls=iref:nba:gnav |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |Video             |http://www.nba.com/video/?ls=iref:nba:gnav    |An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |
    |Players           |http://stats.nba.com/players/?ls=iref:nba:gnav|An NBA home page|basketball1.jpg|C:\Users\hnguyen\Pictures\basketball1.jpg|2-Menu     |

  Scenario: verify that a menu's title, menu's administrative summary and the weight of its link can be edited

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "2-Menu" on the current page
    Then I click the dropdown button of the href "/admin/structure/menu/manage/2-menu"
    And I enter "An Updated QA Menu2" in the "Title" input box
    And I enter "An Updated QA Menu2 admin summary" in the "Administrative summary" input box
    And I press the "Save" button

    Given I see "Menu An Updated QA Menu2 has been updated." of element "div.messages--status" in the toolbar region

    ## Cannot update a specific link using its href because the item number is
    ## not a constant.  For ex, if href is "/admin/structure/menu/item/49/edit?destination=/admin/structure/menu/manage/2-menu"
    ## for the "Teams" link, after the link is deleted and re-created, its href can be
    ## "/admin/structure/menu/item/3/edit?destination=/admin/structure/menu/manage/2-menu"

    ## And I see the "Teams" link
    ## When I click the dropdown button of the href "/admin/structure/menu/item/49/edit?destination=/admin/structure/menu/manage/2-menu"
    When I click the dropdown button of element "li.dropbutton-action a"
    And I enter "9" in the "Weight" input box
    Then I press the "Save" button
    And I see "The menu link has been saved." of element "div.messages--status" in the toolbar region

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

  Scenario: Set up the environment to test a link with sublinks

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    And I see the "Add menu" link
    When I click the "Add menu" link
    Then I see "Add menu" whose id is "block-seven-page-title" on the current page

    Given I enter "3-Menu" in the "Title" input box

    ## a default machine name can be editted by clicking the Edit link.  This is done
    ## in this scenario because this link is not clicked, when the script executes the
    ## pressing button, it actually click on the Edit link !!
    And I click the "Edit" link whose the class is "link"

    And I enter "3-Menu admin summary" in the "Administrative summary" input box
    When I press the "Save" button
    And I see "Menu 3-Menu has been added." of element "div.messages--status" in the toolbar region

    Given I click the "Add link" link
    When I see "Add menu link" whose id is "block-seven-page-title" on the current page
    Then I enter "Link1" in the "Menu link title" input box
    And I enter "http://www.tbs.com" in the "Link" input box
    And I enter "Link1 Description" in the "Description" input box
    And I select the parent link of "Main navigation"
    And I enter "-100" in the "Weight" input box
    Then I press the "Save" button
    And I see "The menu link has been saved." of element "div.messages--status" in the toolbar region

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

  Scenario Outline: verify that a link can be a parent of another link

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "3-Menu" on the current page
    Then I click the dropdown button of the href "/admin/structure/menu/manage/3-menu"

    Given I click the "Add link" link
    When I see "Add menu link" whose id is "block-seven-page-title" on the current page
    Then I enter "<Link Title>" in the "Menu link title" input box
    And I enter "http://www.tbs.com" in the "Link" input box
    And I enter "Link Description" in the "Description" input box
    And I select the parent link of "<Parent Link>"
    And I enter "-100" in the "Weight" input box
    And I press the "Save" button
    And I see "The menu link has been saved." of element "div.messages--status" in the toolbar region

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
      |Link Title|Parent Link|
      |Link2     |Link1      |
      |Link3     |Link2      |
      |Link4     |Link3      |
      |Link5     |Link4      |
      |Link6     |Link5      |
      |Link7     |Link6      |

  Scenario: verify that a link with sublinks is displayed in teh Main navigation block

    Given I see the subnav "Link1" link
    And I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

  Scenario: verify that a link can be added to teh Main navigation menu

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "Main navigation" on the current page
    Then I click the dropdown button of the href "/admin/structure/menu/manage/main"

    Given I click the "Add link" link
    When I see "Add menu link" whose id is "block-seven-page-title" on the current page
    Then I enter "Turner" in the "Menu link title" input box
    And I enter "http://www.turner.com" in the "Link" input box
    And I enter "a test link" in the "Description" input box
    And I enter "-100" in the "Weight" input box
    And I press the "Save" button

    Given I see "The menu link has been saved." of element "div.messages--status" in the toolbar region
    When I click the "Back to site" link
    Then I see the subnav "Turner" link

    Given I click the "Turner" link
    When I see the "Company" link
    Then I click the back arrow of browser

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

  #Scenario: delete the subnav link for future test

  #  Given I search for the "Structure" link by clicking the "Manage" toolbar icon
  #  And I see the "Structure" link
  #  When I click the "Structure" link
  #  Then I see the "Menus" link

  #  Given I click the "Menus" link from a list of link
  #  When I see the cell name "Main navigation" on the current page
  #  Then I click the dropdown button of the href "/admin/structure/menu/manage/main"
  #  And I see the "Turner" link
  #  And I click the dropdown button of the xpath ".//*[@id='menu-overview']/tbody/tr[1]/td[4]/div/div/ul/li[1]/a"

  #  Given I see the "Delete" link
  #  When I click the "Delete" link
  #  Then I press the "Delete" button

  #  Given I click the "marty" toolbar icon
  #  When I click the "Log out" link
  #  Then I close the web browser

  Scenario Outline: verify that a menu can be deleted

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "<Menu Name>" on the current page
    Then I click the dropdown button of the href "<Href>"
    And I see the "Delete" link
    And I click the "Delete" link
    And I press the "Delete" button
    And I see "<Status Message>" of element "div.messages--status" in the toolbar region

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
    |Menu Name|Href                               |Status Message                   |
    |1-Menu   |/admin/structure/menu/manage/1-menu|The menu 1-Menu has been deleted.|
    |3-Menu   |/admin/structure/menu/manage/3-menu|The menu 3-Menu has been deleted.|
    ###|4-Menu   |/admin/structure/menu/manage/4-menu|The menu 4-Menu has been deleted.|
    ###|5-Menu   |/admin/structure/menu/manage/5-menu|The menu 5-Menu has been deleted.|
    ###|6-Menu   |/admin/structure/menu/manage/6-menu|The menu 6-Menu has been deleted.|
    ###|7-Menu   |/admin/structure/menu/manage/7-menu|The menu 7-Menu has been deleted.|
    ###|8-Menu   |/admin/structure/menu/manage/8-menu|The menu 8-Menu has been deleted.|

  Scenario Outline: verify that 7 links can be deleted

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "<Menu Name>" on the current page
    Then I click the dropdown button of the href "<Href Menu>"
    And I see the "<Link Title>" link
    And I click the dropdown button of the xpath ".//*[@id='menu-overview']/tbody/tr[1]/td[4]/div/div/ul/li[1]/a"

    Given I see the "Delete" link
    When I click the "Delete" link
    Then I press the "Delete" button

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
      |Menu Name          |Link Title        |        Href Menu                  |
      |An Updated QA Menu2|Players           |/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|Playoffs          |/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|Scores & Schedules|/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|Teams             |/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|Tickets           |/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|Video             |/admin/structure/menu/manage/2-menu|
      |An Updated QA Menu2|News              |/admin/structure/menu/manage/2-menu|
      |Main navigation    |Link1             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link2             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link3             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link4             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link5             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link6             |/admin/structure/menu/manage/main  |
      |Main navigation    |Link7             |/admin/structure/menu/manage/main  |
      |Main navigation    |Turner            |/admin/structure/menu/manage/main  |

  Scenario Outline: delete the test menu for future test

    Given I search for the "Structure" link by clicking the "Manage" toolbar icon
    And I see the "Structure" link
    When I click the "Structure" link
    Then I see the "Menus" link

    Given I click the "Menus" link from a list of link
    When I see the cell name "<Menu Name>" on the current page
    Then I click the dropdown button of the href "<Href>"
    And I see the "Delete" link
    And I click the "Delete" link
    And I press the "Delete" button

    Given I click the "marty" toolbar icon
    When I click the "Log out" link
    Then I close the web browser

    Examples:
    |Menu Name          |Href                               |
    |An Updated QA Menu2|/admin/structure/menu/manage/2-menu|
